//
//  ViewController.swift
//  SwiftChat
//
//  Created by Gosha on 22/09/2018.
//  Copyright © 2018 Georgii Rozhnev. All rights reserved.
//

/*
 Add methods
 
 viewWillAppear
 viewDidAppear
 viewWillLayoutSubviews
 viewDidLayoutSubviews
 viewWillDisappear
 viewDidDisappear
 */

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        print("\(#function)\n")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        print("\(#function)\n")
    }
    
    func viewWillAppear(){
        print("\(#function)\n")
    }
    func viewDidAppear(){
        print("\(#function)\n")
    }
    override func viewWillLayoutSubviews(){
        print("\(#function)\n")
    }
    override func viewDidLayoutSubviews(){
        print("\(#function)\n")
    }
    func viewWillDisappear(){
        print("\(#function)\n")
    }
    func viewDidDisappear(){
        print("\(#function)\n")
    }
}

