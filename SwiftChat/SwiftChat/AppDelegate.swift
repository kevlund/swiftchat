//
//  AppDelegate.swift
//  SwiftChat
//
//  Created by Gosha on 22/09/2018.
//  Copyright © 2018 Georgii Rozhnev. All rights reserved.
//

/*
 States:
 
 Non-running - The app is not running.
 Inactive - The app is running in the foreground, but not receiving events. An iOS app can be placed into an inactive state, for example, when a call or SMS message is received.
 Active - The app is running in the foreground, and receiving events.
 Background - The app is running in the background, and executing code.
 Suspended - The app is in the background, but no code is being executed.
 */

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

//1
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        print("Application moved from <Non-running> to <Inactive>: \(#function)\n")
        
        return true
    }
    
    //2
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.

        print("Application moved from <Inactive> to <Active>: \(#function)\n")
    }

    //3
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.

        print("Application moved from <Active> to <Inactive>: \(#function)\n")
    }

    //4
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.

        print("Application moved from <Inactive> to <Background>: \(#function)\n")
    }

    //5
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.

        print("Application moved from <Background> to <Inactive>: \(#function)\n")
    }

 
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        
        print("Application moved from <> to <Non-running>: \(#function)\n")
    }


}

